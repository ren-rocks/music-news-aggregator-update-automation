const Parser = require('rss-parser');
const AWS = require('aws-sdk');
const _ = require('lodash');
const TableName = process.env.DYNAMODB_TABLE;

const getTodaysKey = () => (new Date()).getUTCDate() + '/' + ((new Date()).getUTCMonth() + 1) + '/' + (new Date()).getUTCFullYear();

const getRedditStories = async subreddit => {
  const parser = new Parser();
  const feed = await parser.parseURL(`https://www.reddit.com/r/${subreddit}/rising/.rss?sort=rising`);

  let items = {};
  for (let i = 0; i < feed.items.length; i++) {
    let item = feed.items[i];
    const regex = /<a.*?href=\"([^\"\']*?)\">\[link\]<\/a>/ig;
    m = regex.exec(item.content);

    switch (true) {
      case m.length < 2:
      case m[1].indexOf('reddit') >= 0:
      case m[1].indexOf('soundcloud') >= 0:
      case m[1].indexOf('bandcamp') >= 0:
      case m.title.toLowerCase().indexOf('how do i '):
        continue;
    }

    items[m[1]] = {
      title: item.title,
      published: Date.parse(item.pubDate)
    };
  }

  return items;
};


const getHNStories = async keyword => {
  const parser = new Parser();
  const feed = await parser.parseURL(`https://hnrss.org/newest?q=${keyword}`);

  let items = {};
  for (let i = 0; i < feed.items.length; i++) {
    let item = feed.items[i];
    if (item.link.indexOf('ycombinator') >= 0) {
      continue;
    }

    items[item.link] = {
      title: item.title,
      published: Date.parse(item.pubDate)
    };
  }

  return items;
};

const getCurrentItems = async () => {
  const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

  let items = {};
  const data = await dynamoDB.getItem({
    TableName,
    Key: {
      "date": {
        S: getTodaysKey()
      }
    },
  }).promise();

  if (typeof data.Item !== 'undefined') {
    items = JSON.parse(data.Item.data.S);
  }

  return items;
};

const setNewItems = async items => {
  const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
  const params = {
    TableName,
    Item: {}
  };
  params.Item.date = {};
  params.Item.data = {};
  params.Item.date.S = getTodaysKey();
  params.Item.data.S = JSON.stringify(items);
  const res = await dynamoDB.putItem(params).promise();

  return res;
};

const searchHN = async keyword => {
  const hnItems = await getHNStories(keyword);
  let items = await getCurrentItems();
  _.merge(items, hnItems);
  await setNewItems(items).catch(err => err);
};

const searchReddit = async keyword => {
  const hnItems = await getHNStories(keyword);
  let items = await getCurrentItems();
  _.merge(items, hnItems);
  await setNewItems(items).catch(err => err);
  return 'success';
};

module.exports.getHN = async e => {
  await searchHN('dj');
  await searchHN('music production');
  return 'success';
};

module.exports.getReddit = async e => {
  await searchReddit('DJs');
  await searchReddit('musicproduction');
  await searchReddit('MusicProductionDeals');
  return 'success';
};
