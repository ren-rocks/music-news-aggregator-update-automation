const AWS = require('aws-sdk');
const fs = require('fs');
const Handlebars = require('handlebars');

const getIconTag = url => {
  let a = url.split('/');
  if (a.length < 3) {
    return '';
  }

  return `<img src="https://favicons.githubusercontent.com/${a[2]}" style="height:14px; margin: 0px 10px;" alt="${a[2]}">`;
};

const getTodaysKey = () => (new Date()).getUTCDate() + '/' + ((new Date()).getUTCMonth() + 1) + '/' + (new Date()).getUTCFullYear();

const createLinkHTMLTag = item => `<li style="margin-bottom: .5em;"><a class="text-info" href="${item.url}" target="_blank" >${item.title}</a >${getIconTag(item.url)}</li > `;

const updateSite = async event => {
  const Bucket = process.env.S3_BUCKET;
  const TableName = process.env.DYNAMODB_TABLE;
  const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
  const data = await dynamoDB.getItem({
    TableName,
    Key: { "date": { S: getTodaysKey() } },
  }).promise();

  if (typeof data.Item === 'undefined') {
    throw new Error(`no item for date ${getTodaysKey()} was found.`);
  }

  let items = JSON.parse(data.Item.data.S);
  items = Object.keys(items).map(key => {
    items[key].url = key;
    return items[key];
  });

  _.remove(items, i => typeof i.title === 'undefined' || i.title === 'undefined');

  items = _.orderBy(items, ['published'], 'desc');

  let markup = '';
  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    markup += createLinkHTMLTag(item);

    if (i === 49) {
      break;
    }
  }

  if (markup === '') {
    throw new Error(`no items for date ${getTodaysKey()} were found and markup returned empty. not updating.`);
  }

  const footer = `Last updated ${(new Date()).toUTCString()}`;
  const templateContent = await fs.readFileSync('./templates/index.html').toString();
  const template = Handlebars.compile(templateContent);
  const Body = template({ markup, footer });

  const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
  const res = await s3.putObject({
    Body,
    Bucket,
    Key: 'index.html',
    ACL: 'public-read',
    ContentType: 'text/html; charset=UTF-8',
    Metadata: {
      'Content-Type': 'text/html'
    }
  }).promise();

  if (res.statusCode >= 200 && res.statusCode < 300) {
    throw new Error(`response from s3 putObject is ${res.statusCode}`);
  }
};

module.exports.updateSite = async e => {
  await updateSite();
  console.log('success');
};